# Pokémon GO Pokédex

This is a React application that connects to a API web service and returns data for Pokédex of Pokémon GO in JSON format.

API location: - https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json

## Installation

- Please clone the repo and open in an IDE, works best with Visual Studio Code.
- Open a terminal and run `npm i` to install the dependencies.
- Then run `npm start` to load the project.
- The project will then be available at [localhost:3000](http://localhost:3000/)

## Technologies & Languages

- JS, React, Typescript, React Router, Axios, Reactstrap, React Highlighter, SCSS, Bootstrap, Google Fonts, Lineicons

## Notes

* The data is retrieved asynchronously using an axios call and then stored in a local state object `results` via the use of a `useState` and `useEffect`.
* A loading component is displayed until the data has been retrieved.
* Once the data exists, the table will be rendered on screen.
* The main table is located in the `ViewMain.tsx` file and is made up of 3 components: `outputTableHead`, `outputTableRows` and is rendered via the `outputTable` component.
  1. `outputTableHead` - the markup for the table headers
  2. `outputTableRows` - the markup for the table rows, achieved by mapping through the results stored in the local state object.
  3. `outputTable` - combines the above components.
* There is also a card display option and the cards are also located in the same file under the `outputCards` component.
* The data is typed and these interfaces are stored in the `typePokemon` file.
* Router is used to provide a child page for each pokemon listed in the table.
* Bootstrap, Reactstrap, Lineicons and Google fonts are being used to provide a foundation for styles.  Then custom scss is imported inline using the default React method.
