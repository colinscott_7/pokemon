import * as React from "react";

export const useThrottledValue = <T>(value: T, timerInMs = 250): T => {
  const [throttledValue, setThrottledValue] = React.useState<T>(value);
  const timeout = React.useRef<any>();

  React.useEffect(() => {
    if (timeout.current) {
      clearTimeout(timeout.current);
      timeout.current = setTimeout(() => {
        setThrottledValue(value);
        timeout.current = null;
      }, timerInMs);
    } else {
      setThrottledValue(value);
      timeout.current = setTimeout(() => {
        timeout.current = null;
      }, timerInMs);
    }
  }, [value]);

  return throttledValue;
};
