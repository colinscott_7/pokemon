import { Loading } from "./components";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import "./scss/index.scss";

import { ViewMain } from "./views/ViewMain";
import { ViewDetails } from "./views/ViewDetails";
import React from "react";
import { typePokemon } from "./interfaces/typePokemon";
import axios from "axios";

export const App = () => {
  const [results, setResults] = React.useState<typePokemon[]>();

  const getData = async () => {
    const { data } = await axios(
      "https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json"
    );
    setResults(data.pokemon);
    // test loading animation
    // setTimeout(() => setResults(data.pokemon), 3000);
  };

  React.useEffect(() => {
    getData();
  }, []);

  if (!results) {
    return <Loading />;
  }
  const getPokemonRoutes = () =>
    results?.map((page, index) => {
      return (
        <Route path={`/${page.num}`} component={() => ViewDetails(page)} key={`route___${index}`} />
      );
    });

  return (
    <>
      <Router>
        <Switch>
          <Route path={"/"} exact component={() => ViewMain(results)} />
          {getPokemonRoutes()}
        </Switch>
      </Router>
    </>
  );
};
