import { useHistory } from "react-router-dom";
import { typePokemon, typePokemonEvo } from "../interfaces/typePokemon";
import { Loading } from "../components";
import * as rs from "reactstrap";

export const ViewDetails = (props: typePokemon) => {
  const history = useHistory();

  if (!props) {
    return <Loading />;
  }
  const types = props.type.map((item: string) => {
    return <span className="badge bg-secondary me-2">{item}</span>;
  });
  const multipliers = props.multipliers?.map((item: number) => {
    return <span className="badge bg-success me-2">{item.toString()}</span>;
  });
  const weaknesses = props.weaknesses?.map((item: string) => {
    return <span className="badge bg-danger me-2">{item}</span>;
  });
  const prev_evolution = props.prev_evolution?.map((item: typePokemonEvo) => {
    return (
      <tr>
        <td>
          <rs.Button color="link" className="p-0" onClick={() => history.push(`/${item.num}`)}>
            {item.num}
          </rs.Button>
        </td>
        <td>
          <rs.Button color="link" className="p-0" onClick={() => history.push(`/${item.num}`)}>
            {item.name}
          </rs.Button>
        </td>
      </tr>
    );
  });
  const next_evolution = props.next_evolution?.map((item: typePokemonEvo) => {
    return (
      <tr>
        <td>
          <rs.Button color="link" className="p-0" onClick={() => history.push(`/${item.num}`)}>
            {item.num}
          </rs.Button>
        </td>
        <td>
          <rs.Button color="link" className="p-0" onClick={() => history.push(`/${item.num}`)}>
            {item.name}
          </rs.Button>
        </td>
      </tr>
    );
  });
  return (
    <>
      <div className="wrapper">
        <div className="content view-details">
          <rs.Container>
            <rs.Row>
              <rs.Col>
                <h1>Pokémon GO Pokédex</h1>
                <rs.Button
                  color="primary"
                  className="text-white ms-2 mb-3 "
                  onClick={() => history.goBack()}
                >
                  Go back
                </rs.Button>
              </rs.Col>
            </rs.Row>
            <rs.Row className="gx-4">
              <rs.Col sm={12} md={12} lg={7}>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <rs.Row>
                      <rs.Col sm={6} md={4} lg={6} xl={4}>
                        <img className="img-thumbnail  " src={props.img} />
                      </rs.Col>
                      <rs.Col sm={6} md={8} lg={6} xl={8}>
                        <small>Name</small>
                        <h2 className="h4">{props.name}</h2>
                        <rs.Row className="my-3">
                          <rs.Col>
                            <small>Height</small>
                            <p className="mb-0">{props.height}</p>
                          </rs.Col>
                          <rs.Col>
                            <small>Weight</small>
                            <p className="mb-0">{props.weight}</p>
                          </rs.Col>
                        </rs.Row>
                        <small>Types</small>
                        <p className="mb-0">{types}</p>
                      </rs.Col>
                    </rs.Row>
                  </rs.CardBody>
                </rs.Card>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <rs.Row>
                      <rs.Col>
                        <small>Candy</small>
                        <p>{props.candy}</p>
                      </rs.Col>
                      <rs.Col>
                        <small>Candy count</small>
                        <p>{props.candy_count ? props.candy_count : "N/A"}</p>
                      </rs.Col>
                      <rs.Col>
                        <small>Egg</small>
                        <p>{props.egg}</p>
                      </rs.Col>
                    </rs.Row>
                  </rs.CardBody>
                </rs.Card>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <rs.Row>
                      <rs.Col>
                        <small>Spawn chance</small>
                        <p>{props.spawn_chance.toString()}</p>
                      </rs.Col>
                      <rs.Col>
                        <small>Avg spawns</small>
                        <p>{props.avg_spawns.toString()}</p>
                      </rs.Col>
                      <rs.Col>
                        <small>Spawn time</small>
                        <p>{props.spawn_time.toString()}</p>
                      </rs.Col>
                    </rs.Row>
                  </rs.CardBody>
                </rs.Card>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <rs.Row>
                      <rs.Col>
                        <small className="d-block">Multipliers</small>
                        {props.multipliers ? multipliers : "N/A"}
                      </rs.Col>
                      <rs.Col>
                        <small className="d-block">Weaknesses</small>
                        {props.weaknesses ? weaknesses : "N/A"}
                      </rs.Col>
                    </rs.Row>
                  </rs.CardBody>
                </rs.Card>
              </rs.Col>
              <rs.Col sm={12} md={12} lg={5}>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <small className="d-block">Previous evolution</small>
                    {props.prev_evolution ? (
                      <rs.Table bordered>
                        <thead className="table-dark">
                          <tr>
                            <th>Number</th>
                            <th>Name</th>
                          </tr>
                        </thead>
                        <tbody>{prev_evolution}</tbody>
                      </rs.Table>
                    ) : (
                      "N/A"
                    )}
                  </rs.CardBody>
                </rs.Card>
                <rs.Card className="animate__animated animate__fadeIn">
                  <rs.CardBody className="text-start">
                    <small className="d-block">Next evolution</small>
                    {props.next_evolution ? (
                      <rs.Table bordered>
                        <thead className="table-dark">
                          <tr>
                            <th>Number</th>
                            <th>Name</th>
                          </tr>
                        </thead>
                        <tbody>{next_evolution}</tbody>
                      </rs.Table>
                    ) : (
                      "N/A"
                    )}
                  </rs.CardBody>
                </rs.Card>
              </rs.Col>
            </rs.Row>
          </rs.Container>
        </div>
      </div>
    </>
  );
};
