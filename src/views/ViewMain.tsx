import React from "react";
import { useHistory } from "react-router-dom";
import { typePokemon } from "../interfaces/typePokemon";
import * as rs from "reactstrap";
import { Loading } from "../components";
import { useThrottledValue } from "../hooks/useThrottledValue";
import * as Highlight from "react-highlighter";
import { typeTableHeader } from "../interfaces/tableHeaders";

export const ViewMain = (props: typePokemon[]) => {
  const history = useHistory();
  const [sortField, setSortField] = React.useState<string>("Name");
  const [sortDirection, setSortDirection] = React.useState<string>("asc");
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const throttledSearchTerm = useThrottledValue(searchTerm, 300);
  const [displayMode, setDisplayMode] = React.useState<string>("table");

  if (!props) {
    return <Loading />;
  }

  // handlers
  const handleSortData = (field: string, dir: string) =>
    field === "#" && dir === "asc"
      ? (a: any, b: any) => (a.num > b.num ? 1 : a.num < b.num ? -1 : 0)
      : field === "#" && dir === "desc"
      ? (a: any, b: any) => (a.num > b.num ? -1 : a.num < b.num ? 1 : 0)
      : field === "Name" && dir === "asc"
      ? (a: any, b: any) => (a.name > b.name ? 1 : a.name < b.name ? -1 : 0)
      : field === "Name" && dir === "desc"
      ? (a: any, b: any) => (a.name > b.name ? -1 : a.name < b.name ? 1 : 0)
      : field === "Height" && dir === "asc"
      ? (a: any, b: any) => (a.height > b.height ? 1 : a.height < b.height ? -1 : 0)
      : field === "Height" && dir === "desc"
      ? (a: any, b: any) => (a.height > b.height ? -1 : a.height < b.height ? 1 : 0)
      : field === "Weight" && dir === "asc"
      ? (a: any, b: any) => (a.weight > b.weight ? 1 : a.weight < b.weight ? -1 : 0)
      : field === "Weight" && dir === "desc"
      ? (a: any, b: any) => (a.weight > b.weight ? -1 : a.weight < b.weight ? 1 : 0)
      : field === "Candy" && dir === "asc"
      ? (a: any, b: any) => (a.candy > b.candy ? 1 : a.candy < b.candy ? -1 : 0)
      : field === "Candy" && dir === "desc"
      ? (a: any, b: any) => (a.candy > b.candy ? -1 : a.candy < b.candy ? 1 : 0)
      : undefined;
  const handleSortClick = (field: string) => {
    setSortField(field);
    const sortDir = sortDirection === "" ? "asc" : sortDirection === "asc" ? "desc" : "";
    setSortDirection(sortDir);
  };
  const handleDisplayMode = () => {
    setDisplayMode(displayMode === "card" ? "table" : "card");
  };
  const handleResetDefaults = () => {
    setSortField("Name");
    setSortDirection("asc");
    setSearchTerm("");
  };

  // functions
  const getDisplayBtnLabel = () =>
    displayMode === "card" ? (
      <>
        Table view <i className="las la-th-list"></i>
      </>
    ) : (
      <>
        Card view <i className="las la-th-large"></i>
      </>
    );

  // get data and apply filters/sort
  const data = props
    .filter(
      (item: typePokemon) =>
        item.name.toLowerCase().includes(throttledSearchTerm.toLowerCase()) ||
        item.candy.toLowerCase().includes(throttledSearchTerm.toLowerCase())
    )
    .sort(handleSortData(sortField, sortDirection));

  // show loading until data is loaded
  if (!data) {
    return <Loading />;
  }

  // components
  const tableHeaders: typeTableHeader[] = [
    { label: "#", sortable: true, width: 75 },
    { label: "Image", sortable: false, width: 100 },
    { label: "Name", sortable: true },
    { label: "Height", sortable: true, width: 120 },
    { label: "Weight", sortable: true, width: 120 },
    { label: "Type(s)", sortable: false },
    { label: "Candy", sortable: true, width: 275 },
  ];
  const outputTableHead = () => {
    const output = tableHeaders?.map((headerCell: typeTableHeader, index: number) => {
      const headerLink = (
        <rs.Button color="link" className="p-0" onClick={() => handleSortClick(headerCell.label)}>
          {headerCell.label}{" "}
          {sortField === headerCell.label ? outputSortIcon() : <i className="las la-sort"></i>}
        </rs.Button>
      );
      return (
        <th key={`videoHeadCell_${index}`} style={{ width: `${headerCell.width}px` }}>
          {headerCell.sortable ? headerLink : headerCell.label}
        </th>
      );
    });
    return output;
  };
  const outputTableRows = () => {
    const output = data?.map((pokemon: typePokemon, index: number) => {
      const pokemonTypes = pokemon.type.map((pokemonType: string) => (
        <span className="badge bg-secondary me-2">{pokemonType}</span>
      ));
      return (
        <tr key={`videoRow_${index}`}>
          <td>{pokemon.num}</td>
          <td>
            <rs.Button color="link" className="p-0" onClick={() => history.push(pokemon.num)}>
              <img src={pokemon.img} width={40} />
            </rs.Button>
          </td>
          <td>
            <rs.Button color="link" onClick={() => history.push(pokemon.num)}>
              <span className="fw-bold">
                <Highlight search={throttledSearchTerm}>{pokemon.name}</Highlight>
              </span>
            </rs.Button>
          </td>
          <td>{pokemon.height}</td>
          <td>{pokemon.weight}</td>
          <td>{pokemonTypes}</td>
          <td>
            <Highlight search={throttledSearchTerm}>{pokemon.candy}</Highlight>
          </td>
        </tr>
      );
    });
    return output;
  };
  const outputSortIcon = () => {
    const output =
      sortDirection === "asc" ? (
        <i className="las la-sort-up"></i>
      ) : sortDirection === "desc" ? (
        <i className="las la-sort-down"></i>
      ) : (
        <i className="las la-sort"></i>
      );
    return output;
  };
  const outputCards = () => {
    const output = data?.map((pokemon: typePokemon, index: number) => {
      const pokemonTypes = pokemon.type.map((pokemonType: string) => (
        <span className="badge bg-secondary me-2">{pokemonType}</span>
      ));
      return (
        <rs.Col sm={6} lg={4} xl={3} xxl={2} className="mb-4" key={`videoCard_${index}`}>
          <rs.Button
            color="link"
            className="card-btn"
            onClick={() => history.push(`/${pokemon.num}`)}
          >
            <rs.Card className="w-100 h-100 animate__animated animate__fadeIn">
              <div className="card-img-top">
                <img src={pokemon.img} />
              </div>
              <rs.CardBody>
                <rs.Row className="mb-2">
                  <rs.Col sm={12}>
                    <small>Name</small>
                  </rs.Col>
                  <rs.Col>
                    <h2 className="card-title text-decoration-underline text-primary">
                      <Highlight search={throttledSearchTerm}>{pokemon.name}</Highlight>
                    </h2>
                  </rs.Col>
                </rs.Row>
                <rs.Row className="mb-2">
                  <rs.Col sm={12}>
                    <small>Height</small>
                  </rs.Col>
                  <div className="col">
                    <span>{pokemon.height}</span>
                  </div>
                </rs.Row>
                <rs.Row className="mb-2">
                  <rs.Col sm={12}>
                    <small>Weight</small>
                  </rs.Col>
                  <div className="col">
                    <span>{pokemon.weight}</span>
                  </div>
                </rs.Row>
                <rs.Row className="mb-3">
                  <rs.Col sm={12}>
                    <small>Types</small>
                  </rs.Col>
                  <div className="col">{pokemonTypes}</div>
                </rs.Row>
                <rs.Row>
                  <rs.Col sm={12}>
                    <small>Candy</small>
                  </rs.Col>
                  <div className="col">
                    <Highlight search={throttledSearchTerm}>{pokemon.candy}</Highlight>
                  </div>
                </rs.Row>
              </rs.CardBody>
            </rs.Card>
          </rs.Button>
        </rs.Col>
      );
    });
    if (output.length === 0) {
      return (
        <>
          <h3>Sorry, no results were found for the query "{throttledSearchTerm}"</h3>
        </>
      );
    }
    return <rs.Row>{output}</rs.Row>;
  };
  const outputSortDropdown = () => {
    const ddItems = tableHeaders
      ?.filter((item: typeTableHeader) => item.sortable === true)
      .map((item: typeTableHeader, index) => {
        return (
          <rs.DropdownItem onClick={() => handleSortClick(item.label)} key={`ddItem___${index}`}>
            {item.label}
            {sortField === item.label ? outputSortIcon() : <i className="las la-sort"></i>}
          </rs.DropdownItem>
        );
      });
    return (
      <rs.UncontrolledButtonDropdown>
        <rs.DropdownToggle
          caret
          color="dark"
          className="text-white animate__animated animate__fadeIn"
        >
          Sort by
        </rs.DropdownToggle>
        <rs.DropdownMenu>{ddItems}</rs.DropdownMenu>
      </rs.UncontrolledButtonDropdown>
    );
  };
  const outputTable = (
    <div className="table-responsive animate__animated animate__fadeIn">
      <rs.Table id="products" striped bordered hover>
        <thead className="table-dark">{outputTableHead()}</thead>
        <tbody>{outputTableRows()}</tbody>
      </rs.Table>
    </div>
  );

  // check if the default list display has been modified
  const listModified = sortField !== "Name" || sortDirection !== "asc" || searchTerm !== "";

  // OUTPUT
  return (
    <>
      <div className="wrapper">
        <div className="content">
          <rs.Container>
            <rs.Row>
              <rs.Col>
                <h1>Pokémon GO Pokédex</h1>
              </rs.Col>
            </rs.Row>
            <rs.Row className="align-items-start">
              <rs.Col md={6}>
                <rs.FormGroup>
                  <rs.Label htmlFor="formValueSearch" className="form-label d-none">
                    Search
                  </rs.Label>
                  <rs.Input
                    type="search"
                    id="formValueSearch"
                    placeholder="Enter search term..."
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.currentTarget.value)}
                  />
                </rs.FormGroup>
              </rs.Col>
              <rs.Col>
                {displayMode === "card" && outputSortDropdown()}
                {listModified && (
                  <rs.Button
                    color="primary"
                    className="text-white ms-2 animate__animated animate__fadeIn"
                    onClick={() => handleResetDefaults()}
                  >
                    Reset to default
                  </rs.Button>
                )}
              </rs.Col>
              <rs.Col md={2} className="text-end">
                <rs.Button
                  color="secondary"
                  className="ms-auto animate__animated animate__fadeIn"
                  onClick={() => handleDisplayMode()}
                >
                  {getDisplayBtnLabel()}
                </rs.Button>
              </rs.Col>
            </rs.Row>
            <rs.Row>
              <rs.Col>
                <div className="display-details d-flex mb-2 align-items-start justify-content-end">
                  {searchTerm !== "" && (
                    <div className="d-flex align-items-center me-3 animate__animated animate__fadeIn">
                      <small className="me-1">Searching for:</small>
                      <span>"{searchTerm}"</span>
                    </div>
                  )}
                  {sortField !== "" && (
                    <div className="d-flex align-items-center me-3 animate__animated animate__fadeIn">
                      <small className="me-1">Sorted by:</small>
                      <span>"{sortField}"</span>
                    </div>
                  )}
                  {sortDirection !== "" && (
                    <div className="d-flex align-items-center me-3 animate__animated animate__fadeIn">
                      <small className="me-1">Order:</small>
                      <span>"{sortDirection}"</span>
                    </div>
                  )}
                  {data && (
                    <div className="d-flex align-items-center me-3 animate__animated animate__fadeIn">
                      <small className="me-1">Displaying:</small>
                      <span>
                        {data.length} of {props.length} results
                      </span>
                    </div>
                  )}
                </div>
              </rs.Col>
            </rs.Row>

            {displayMode === "card" ? outputCards() : outputTable}
          </rs.Container>
        </div>
      </div>
    </>
  );
};
