export interface typeTableHeader {
  label: string;
  sortable?: boolean;
  width?: number;
}
